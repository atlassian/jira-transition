import jira
import yaml

from bitbucket_pipes_toolkit import Pipe


schema = {
    'JIRA_BASE_URL': {'type': 'string', 'required': True},
    'JIRA_USER_EMAIL': {'type': 'string', 'required': True},
    'JIRA_API_TOKEN': {'type': 'string', 'required': True},
    'ISSUE': {'type': 'string', 'required': True},
    'TRANSITION': {'type': 'string', 'required': True},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False}
}


class JiraTransitionPipe(Pipe):

    def __init__(self, pipe_metadata=None, schema=None, env=None, check_for_newer_version=False):
        super().__init__(
            pipe_metadata=pipe_metadata,
            schema=schema,
            env=env,
            check_for_newer_version=check_for_newer_version
        )
        self.jira_url = self.get_variable('JIRA_BASE_URL')
        self.email = self.get_variable('JIRA_USER_EMAIL')
        self.api_token = self.get_variable('JIRA_API_TOKEN')
        self.issue = self.get_variable('ISSUE')
        self.transition = self.get_variable('TRANSITION')

        self.debug = self.get_variable('DEBUG')

    def get_jira_client_instance(self):
        self.log_info('Initializing a Jira client')

        try:
            jira_client_instance = jira.JIRA(
                self.jira_url,
                basic_auth=(self.email, self.api_token),
                max_retries=0,
                validate=True
            )
        except jira.JIRAError as e:
            self.fail(message=f'Failed to initialize a Jira client for {self.jira_url}: {e}')

        return jira_client_instance

    def run(self):
        self.log_info('Getting an issue')
        # create an instance of the Jira
        jira_client_instance = self.get_jira_client_instance()

        try:
            jira_client_instance.issue(id=self.issue)
        except jira.JIRAError as e:
            self.fail(message=f'Failed to retrieve issue from Jira: {e}')

        if self.debug:
            self.debug('Getting a list of possible transitions.')

            try:
                transitions = jira_client_instance.transitions(issue=self.issue)
            except jira.JIRAError as e:
                self.fail(message=f'Failed to retrieve transitions from Jira: {e}')

            self.debug(f'List of transitions: {[transition["name"] for transition in transitions]}')

        self.log_info(f'Transitioning an issue to {self.transition}')

        try:
            jira_client_instance.transition_issue(
                issue=self.issue,
                transition=self.transition
            )
        except jira.JIRAError as e:
            self.fail(message=f'Failed to perform transition: {e}')

        self.success(message=f'✔ Pipe has finished successfully. Issue transitioned to: {self.transition}')


if __name__ == '__main__':
    with open('/pipe.yml') as f:
        metadata = yaml.safe_load(f.read())
    pipe = JiraTransitionPipe(pipe_metadata=metadata, schema=schema, check_for_newer_version=True)
    pipe.run()
