# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.5.1

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 0.5.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 0.4.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 0.3.0

- minor: Update bitbucket-pipes-toolkit to fix vulnerabilities with certify and gitpython.
- patch: Internal maintenance: bump pipes versions in pipelines config file.

## 0.2.5

- patch: Internal maintenance: pipes base refactored to python. Replace integration tests with unit tests.
- patch: Internal maintenance: update Dockerfile, packages.
- patch: Internal maintenance: update community link.
- patch: Internal maintenance: update release process.

## 0.2.4

- patch: Internal maintenance: change pipe docker image to official one.

## 0.2.3

- patch: Internal maintenance: Add gitignore secrets.
- patch: Internal maintenance: change pipe metadata according to new structure

## 0.2.2

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 0.2.1

- patch: Fix issue

## 0.2.0

- minor: Change default environment variable BITBUCKET_REPO_OWNER to BITBUCKET_WORKSPACE due to deprecation in Bitbucket API.

## 0.1.4

- patch: Updated contributing guidelines

## 0.1.3

- patch: Add analytics

## 0.1.2

- patch: Update readme

## 0.1.1

- patch: Fix error handling

## 0.1.0

- minor: Add issue parsing from ISSUE parameter

## 0.0.6

- patch: Add debug readme

## 0.0.5

- patch: Add dockerhub release

## 0.0.4

- patch: Add examples

## 0.0.3

- patch: Add proper url parsing
- patch: Add proper url parsing and issue extraction from branch name

## 0.0.2

- patch: Add debug logging

## 0.0.1

- patch: Initial
